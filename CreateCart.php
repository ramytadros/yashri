<?php

require_once('Tshirt.php');
require_once('Shoe.php');
require_once('Jacket.php');
require_once('Pants.php');
require_once('Helper.php');

$subtotal = 0;

//we initialize an empty array to which we'll push the discounts texts later on for the bill.
$discounts_texts = array();

$total_discounts = 0;

//we use the following 2 counts to be able to track the the number of tshirts orders for the calculation of jackets discount.
$tshirts_count = 0;
$tshirts_discounts_count = 0;

$request_body = file_get_contents('php://input');
$request = json_decode($request_body);
$items = $request->items;
$currency = $request->currency;

//we reorder the items for the jacket to always be in the beginning of the array for the calculation of the discounts.
usort($items, "reorder");

foreach($items as $item) {
    //we loop on the items in the request, and we create an object of each item, and we pass the currency to the constructor.
    $new_obj = new $item($currency);
    $price = $new_obj->getPrice();

    //if the item is of type shoe, we calculate its discount and we add the string to the discounts_texts array.
    if($item === 'Shoe') {
        $total_discounts += $new_obj->calculateDiscount();
        $discount_text = $new_obj->getDiscountText();
        $discounts_texts[] = $discount_text;
    }

    /*if the item is of type shirt, we calculate the number of tshirts and the number of discounts for the jackets,
        and once the tshirts count hits 2, we increment the tshirts_discount_count and reset the tshirts count back to 0.
    */
    if($item === 'Tshirt') {
        $tshirts_count++;

        if($tshirts_count == 2) {
            $tshirts_discounts_count++;
            $tshirts_count = 0;
        }
    }

    /*if the item is of type jacket, we check the number of tshirts_discount_count and calculate the discount accordingly
        and then we decrement the tshirts_discount_count and so on.
    */
    if($item === 'Jacket') {
        if($tshirts_discounts_count >= 1) {
            $total_discounts += $new_obj->calculateDiscount();
            $discount_text = $new_obj->getDiscountText();
            $discounts_texts[] = $discount_text;
            $tshirts_discounts_count--;
        }
    }

    $subtotal += $price;
}

//we calculate the 14% taxes by using this helper function, you can send whatever percentage you want.
$taxes = Helper::calculateTaxes($subtotal, 14);

$total = $subtotal + $taxes - $total_discounts;

//we start filling the bill object, which will be returned as the response of the API.
$bill = new stdClass();
$bill->subTotal = round($subtotal, 4);
$bill->taxes = round($taxes, 4);

if(!empty($discounts_texts)) {
    $bill->discounts = $discounts_texts;
}

$bill->total = round($total, 4);

$bill = json_encode($bill);
echo $bill;
die;

//this is function that we use to reorder the items to make sure that "Tshirt" items are always at the beginning of the array.
function reorder($a, $b) {
    $order = array("Tshirt", "Jacket", "Pants", "Shoe");
    if ($a == $b) {
        return 0;
    }
    return (array_search($a, $order) < array_search($b, $order)) ? -1 : 1;
}
