<?php

require_once('Helper.php');

class Pants {

    public $price;

    function __construct($currency = NULL, $price = 14.99) {
        $this->price = Helper::convertCurrency($price, $currency);
    }

    function getPrice() {
        $price = $this->price;
        return $price;
    }
}