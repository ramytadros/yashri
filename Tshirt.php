<?php

require_once('Helper.php');

class Tshirt {

    public $price;

    function __construct($currency = NULL, $price = 10.99) {
        $this->price = Helper::convertCurrency($price, $currency);
    }

    function getPrice() {
        $price = $this->price;
        return $price;
    }
}