# Introduction 
This is a small project where a user can add multiple items to the cart and the bill is automatically calculated.

# Getting Started

## Prerequisites
* PHP 7
* Apache or Nginx

## Installing  
These instructions will get you a copy of the project up and running on your local machine.

* Clone the code
```
git clone https://ramytadros@bitbucket.org/ramytadros/yashri.git
``` 

* You can call the following REST API (POST)
```
YOUR_PROJECT_DIRECTORY/CreateCart.php
```
* The request body should look like this
```
{
    "items": ["Tshirt", "Tshirt", "Shoe", "Jacket"],
    "currency": "USD"
}
```

* The currency accept the following values: "USD", "EGP", "EUR"
* The items accept the following values: "Tshirt", "Shoe", "Jacket", "Pants"

## Built Using
* [PHP 7]