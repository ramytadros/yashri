<?php

class Helper
{

    /*
     * parameters: $amount, $currency (Null by default)
     * A helper function that calculates the conversion between the entered currency and USD.
     */
    public static function convertCurrency($amount, $currency = NULL)
    {
        if(!empty($currency)) {
            $conversion_mapping = array(
                'EGP' => 16,
                'EUR' => 0.84,
                'USD' => 1
            );

            $conversion_rate = $conversion_mapping[$currency];
            $converted_amount = $amount * $conversion_rate;
        }
        else {
            $converted_amount = $amount;
        }

        return $converted_amount;
    }

    /*
     * parameters: $amount, $taxes_percentage
     * A helper function that calculates the taxes based on the taxes percentage entered.
     */
    public static function calculateTaxes($amount, $taxes_percentage)
    {
        $taxes = $amount * $taxes_percentage / 100;
        return $taxes;
    }

    public static function getDiscount($amount, $discount_amount) {
        $discount = $amount * $discount_amount / 100;
        return $discount;
    }
}