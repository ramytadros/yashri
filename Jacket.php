<?php

require_once('Helper.php');

class Jacket {
    public $price;
    public $discount;

    function __construct($currency = NULL, $price = 19.99) {
        $this->price = Helper::convertCurrency($price, $currency);
        $this->discount = $this->calculateDiscount();
    }

    function getPrice() {
        $price = $this->price;
        return $price;
    }

    function getDiscountText($discount_percentage = 50) {
        $discount_text = $discount_percentage . '%' . ' off ' . get_class($this) . ': -' . $this->discount;
        return $discount_text;
    }

    function calculateDiscount($discount_amount = 50) {
        $discount = $this->price * $discount_amount / 100;
        return $discount;
    }
}