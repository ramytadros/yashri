<?php

require_once('Helper.php');

class Shoe {
    public $price;
    public $discount;

    function __construct($currency = NULL, $price = 24.99) {
        $this->price = Helper::convertCurrency($price, $currency);
        $this->discount = $this->calculateDiscount();
    }

    function getPrice() {
        $price = $this->price;
        return $price;
    }

    function getDiscountText($discount_percentage = 10) {
        $discount_text = $discount_percentage . '%' . ' off ' . get_class($this) . ': -' . $this->discount;
        return $discount_text;
    }

    function calculateDiscount($discount_percentage = 10) {
        $discount = $this->price * $discount_percentage / 100;
        return $discount;
    }
}